var CONFIG = {
    "client_id": "{{client_id}}",
    "client_secret": "Enter Client Secret here",
    "moduleName": "{{moduleName}}",
    "moduleId": "{{moduleId}}",
    "moduleKey": "{{moduleKey}}",
    "grant_type": "client_credentials",
    "scope": "scope:oauth.token.verify"
}

module.exports = CONFIG;

var http = require('http');
var Q = require('q');
var token = require('../Authentication/AccessToken.js');
var moduleConfig = require('../config.js');
var cache = require('memory-cache');

module.exports = {
    validateToken: function(userToken) {
        var def = Q.defer();
        var accessToken = token.getToken().access_token;
        var userCache = cache.get(userToken);
        if (userCache) {
            def.resolve(userCache);
        } else {
            var options = {
                host: 'api.qa1.nbos.in',
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'X-N-ModuleKey': moduleConfig.moduleKey
                }
            };
            options.path = "/api/oauth/v0/tokens/" + userToken;
            var response = "";
            var req = http.request(options, function(res) {
                res.setEncoding('utf8');
                if (res.statusCode == 404) {
                    def.reject("User not found");
                } else {
                    res.on('data', function(chunk) {
                        response += chunk;
                    });

                    res.on('end', function() {
                        var json = JSON.parse(response);
                        var user = cache.put(json.token, json, 900000, function(key, value) {

                        });
                        def.resolve(json);
                    });
                }
            }, function(res) {
                def.reject("error")
            });

            req.write("hello");
            req.end();
        };




        return def.promise;
    }
};

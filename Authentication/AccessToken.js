var request = require('request');
var Q = require('q');
var buildConfig = require('../config.js')
var accessToken = "";

module.exports = {
    getAccessToken: function() {
        var def = Q.defer();
        var options = {
            url: 'http://api.qa1.nbos.in/oauth/token?client_id='+buildConfig.client_id+"&client_secret="+buildConfig.client_secret+"&grant_type="+buildConfig.grant_type+"&scope="+buildConfig.scope,
            headers: {
                "content-type": "application/json",
            }
        };

        request.post(options, function(error, response, body) {
            console.log(response.statusCode)
            if (!error && response.statusCode == 200) {
                accessToken = JSON.parse(body); // Show the HTML for the Google homepage. 
                def.resolve(body);
            } else {
                def.reject("errror");
            }
        })
        return def.promise;
    },
    getToken: function() {
        return accessToken;
    }
}

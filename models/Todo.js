var mongoose = require('mongoose');

var TodoSchema = new mongoose.Schema({
    memberId: String,
    tenantId: String,
    name: String,
    isCompleted: { type: Boolean, default: false },
    label: String,
    isDelete: { type: Boolean, default: false },
    dueDate: { type: Date, default: Date.now }
});

var LabelSchema = new mongoose.Schema({
    memberId: String,
    label: String
});

var Todos = mongoose.model('Todos', TodoSchema);
var Labels = mongoose.model('Labels', LabelSchema);

module.exports = {
    Labels: Labels,
    Todos: Todos
};

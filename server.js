var express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    routes = require('./routes/index'),
    todos = require('./routes/todo'),

    mongoose = require('mongoose'),
    accessToken = require('./Authentication/AccessToken');

app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, X-Requested-With');
    next();
});

app.get('/', function(req, res, next) {

});

app.post('/', function(req, res, next) {

});
app.use('/', routes);
app.use('/api/todos', todos);

mongoose.connect('mongodb://localhost/starterApp', function(err) {
    if (err) {
        console.log('connection error', err);
    } else {
        accessToken.getAccessToken().then(function(response){
           
        }, function(response){
            console.log("error")
        })
    }
});



module.exports = app;

app.listen('3000');
console.log("server is running at port 3000");

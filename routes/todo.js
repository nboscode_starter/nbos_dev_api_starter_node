var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Models = require('../models/Todo.js');
var authVerify = require('../Authentication/AuthValidation.js');

/* GET all todos listing. */
router.get('/all', function(req, res, next) {
    if (req.headers.authorization) {
        var userToken = req.headers.authorization.split("Bearer ")[1];
        authVerify.validateToken(userToken).then(function(response) {
            console.log(response.tenantId)
            Models.Todos.find({"tenantId":response.tenantId, "isDelete": false }, function(err, todos) {
                if (err) return next(err);
                res.json(todos);
            });
        }, function(error) {
            res.json(error)
        }, function(resp){
            console.log("error");
        });
    } else {
        res.json([{ "error": "no auth header" }]);
    }

});



module.exports = router;
